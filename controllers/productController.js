const Product = require("../models/product.js");

module.exports.addProduct = (reqBody, newData) => {
if(newData.isAdmin == true){
	let newProduct = new Product ({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price

	})
	return newProduct.save().then((newProduct, error) => {
		if(error){
				return error;
		}
		else{
			return newProduct;
		}
	})
}else{
	let message = Promise.resolve('User must be ADMIN to add a new product');
		return message.then((value) => {return value})
	}
}



module.exports.getActiveCourses = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}

// retrieve a single product
module.exports.getSingleProduct = (taskId, newContent) => {
	return Product.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
		return result;
}
	
	})
};

// update product
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , {
			productName: newData.product.productName,
			description: newData.product.description,
			price: newData.product.price,
		}).then((updatedProduct, error) => {
			if(error){
				return false;
			}
			
			return newData;
		
		})

		}
		else {
			let message = Promise.resolve('User must be ADMIN to update a product');
			return message.then((value) => {return value})
	}

}


//archive product
module.exports.archive = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , {
			isActive: newData.product.isActive
		}).then((updatedProduct, error) => {
			if(error){
				return false;
			}
			
			return { message: "Product archived successfully"};
		
		})

		}
		else {
			let message = Promise.resolve('User must be ADMIN to archive product');
			return message.then((value) => {return value})
	}

}

module.exports.getAllProducts= () => {
	return Product.find().then((result) => {
		return result;
	})
}


