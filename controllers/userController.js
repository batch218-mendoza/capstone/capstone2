const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Order = require("../models/order.js");
const Product = require("../models/product.js");

module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})

}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
				if(result == null){
					return false
				}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

				if(isPasswordCorrect){
					return {access: auth.createAccessToken(result)};
				}
				else{
					
					return false;
				}

		}
	})
}


module.exports.changeUserRole = (_id, reqBody) => {
	if(reqBody.isAdmin == true){
		return User.findByIdAndUpdate( _id , {
				isAdmin: reqBody.user.isAdmin
		}).then((updatedAdmin, error) => {
			if(error){
				return false;
			}
			return `UPDATED ROLE ${updatedAdmin}`;
		
		})

		}
		else {
			let message = Promise.resolve('you must be admin to change user role');
			return message.then((value) => {return value})
	}

}


module.exports.getUserProfile = async (taskId) => {

    return User.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
		result.password = " ";
		return result;
}
    
})

}


