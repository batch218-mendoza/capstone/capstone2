
const express = require("express");
const router = express.Router(); 
const auth = require("../auth.js");
const Product = require("../models/product.js");

const productController = require("../controllers/productController.js");

router.get("/addProduct", auth.verify, (req, res) =>{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})


router.get("/getactiveproducts", (req, res) =>{
	productController.getActiveCourses().then(
		resultFromController => {
			res.send(resultFromController)
		})
})

// retrieve single product
router.get("/getSingleProduct/:id", (req,res) => {
productController.getSingleProduct(req.params.id, req.body).then(result => res.send(result));
});

// update product
router.put("/:productId/update", auth.verify, (req, res) =>{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

//archive product

router.put("/:productId/archive", auth.verify, (req, res) =>{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archive(req.params.productId, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

//get all products
router.get("/all", (req,res) => {
productController.getAllProducts().then(result => res.send(result));
});


module.exports = router;
