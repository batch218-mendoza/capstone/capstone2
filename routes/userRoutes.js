const express = require("express");
const router = express.Router();
const User = require("../models/user.js");
const userController =  require("../controllers/userController.js");
const auth = require("../auth.js");
const Product = require("../models/product.js");
const Order = require("../models/order.js");


router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.patch("/:id/changeRole", auth.verify, (req, res) =>{
	const newData = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.changeUserRole(req.params.id, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})


router.get("/:id/userdetails", auth.verify, (req, res) =>{
	userController.getUserProfile(req.params.id).then(
		resultFromController => {
			res.send(resultFromController)
		})
})


module.exports = router;